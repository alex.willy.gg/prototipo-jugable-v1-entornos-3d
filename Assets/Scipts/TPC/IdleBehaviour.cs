using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private float timeUntilIdle;

    [SerializeField]
    private int numberOfIdleAnimations;

    //public string idle_TypeParam = "idle_type";
    private bool isBored;
    private float idleTime;
    private int idle_Type;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ResetIdle();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (isBored == false)
        {
            idleTime += Time.deltaTime;

            if(idleTime > timeUntilIdle && stateInfo.normalizedTime % 1 < 0.02f)
            {
                isBored = true;
                idle_Type = Random.Range(1, numberOfIdleAnimations + 1);
                idleTime = idleTime * 2 - 1;

                animator.SetFloat("idle_type", idle_Type - 1);
            }
        }
        else if (stateInfo.normalizedTime % 1 > 0.98)
        {
            ResetIdle();
        }

        animator.SetFloat("idle_type", idle_Type, 0.5f, Time.deltaTime);
    }
    private void ResetIdle()
    {
        if (isBored)
        {
            idle_Type--;
        }
        isBored = false;
        idleTime = 0;
    }
}
