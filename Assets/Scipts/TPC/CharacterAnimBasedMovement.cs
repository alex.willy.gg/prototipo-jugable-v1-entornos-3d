using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]

public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 3f;
    public float rotationThreshold = 0.3f;
    [Range(0, 180f)]
    public float degreesToTurn = 160f;

    [Header("Animator Parameters")]
    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180Param = "turn180";
    public string jummpingParam = "jummping";

    [Header("Animator Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private Ray wallRay = new Ray();
    private float speed;
    private Vector3 desireMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;
    private bool jummping;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        Cursor.visible = false;
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {
        speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        if (jump)
        {
            animator.SetBool(jummpingParam, true);
        }
        else
        {
            animator.SetBool(jummpingParam, false);
        }
        //Dash only if character has reached maxSpeed (animator parameter value)
        if (speed >= speed - rotationThreshold && dash)
        {
            speed = 1.5f;
        }
        if(speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            //Rotate the character owards desire move direcion based on player input and camera position
            desireMoveDirection = forward * vInput + right * hInput;

            if (Vector3.Angle(transform.forward, desireMoveDirection) >= degreesToTurn) { turn180 = true; }
            else {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desireMoveDirection), rotationSpeed * Time.deltaTime);
            }

            //180 turning
            animator.SetBool(turn180Param, turn180);
            //Move the character
            animator.SetFloat(motionParam, speed, StartAnimTime, Time.deltaTime);
        }
        else if (speed < rotationThreshold)
        {
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            animator.SetFloat(motionParam, speed, StopAnimTime, Time.deltaTime);
        }
    }
    private void OnAnimatorIK(int layerIndex)
    {
        if (speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        //Right foot is front
        if(distanceToRightFoot > distanceToLeftFoot) {mirrorIdle = true;}

        //Right foot is behind
        else { mirrorIdle = false; }
    }
}
